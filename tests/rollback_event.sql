begin;

select plan(6);

-- setup a table in search_path
create table test_rollback(
  id serial primary key,
  description text
);
-- data
insert into test_rollback(description) values ('version 1');
select is((select description from test_rollback), 'version 1');
-- setup audit
select audit.audit_table('test_rollback');
-- alter the value
update test_rollback set description = 'v2';
select is((select description from test_rollback), 'v2');
-- rollback
select audit.rollback_event((select max(event_id) from audit.logged_actions));
select is((select description from test_rollback), 'version 1');


-- setup a table in another schema
create schema foo;
-- setup a table in search_path
create table foo.test_rollback(
  id serial primary key,
  description text
);
-- data
insert into foo.test_rollback(description) values ('foo version 1');
select is((select description from foo.test_rollback), 'foo version 1');
-- setup audit
select audit.audit_table('foo.test_rollback');
-- alter the value
update foo.test_rollback set description = 'foov2';
select is((select description from foo.test_rollback), 'foov2');
-- rollback
select audit.rollback_event((select max(event_id) from audit.logged_actions));
select is((select description from foo.test_rollback), 'foo version 1');

-- with a view
-- FIXME I think commit f5e235f completely broke views, because instead of triggers break the editable views
-- uncomment after fixing this
/*
create view foo.test_view as select id, description from foo.test_rollback;
select is((select description from foo.test_view), 'foo version 1');
select audit.audit_view('foo.test_view', ARRAY['id']);
-- alter the value
update foo.test_view set description = 'v3';
select is((select description from foo.test_view), 'v3');
-- rollback
select audit.rollback_event((select max(event_id) from audit.logged_actions));
select is((select description from foo.test_view), 'foo version 1');
*/


rollback;
